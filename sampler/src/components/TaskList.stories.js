import React from "react";
import { storiesOf } from "@storybook/react";

import TaskList from "./Tasklist";
import { task, actions } from "./Task.stories";

export const defaultTasks = [
  { ...task, id: "1", title: "task 1" },
  { ...task, id: "2", title: "task 2" },
  { ...task, id: "3", title: "task 3" },
  { ...task, id: "4", title: "task 4" },
  { ...task, id: "5", title: "task 5" },
  { ...task, id: "6", title: "task 6" }
];

export const withPinnedTasks = [
  ...defaultTasks.slice(0, 5),
  { id: "6", state: "TASK_PINNED", title: "task 6 (pinned)" }
];

storiesOf("task liist", module)
  .addDecorator(story => <div style={{ padding: "3rem" }}>{story()}</div>)
  .add("default", () => <TaskList tasks={defaultTasks} {...actions} />)
  .add("withPinnedTasks", () => (
    <TaskList tasks={withPinnedTasks} {...actions} />
  ))
  .add("loading", () => <TaskList tasks={[]} {...actions} />)
  .add("empty", () => <TaskList tasks={[]} {...actions} />);
